FROM python:3.10.6-alpine

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

WORKDIR /app

COPY . /app/
# COPY requirements.txt /app/
# RUN python3 -m venv env
# RUN source env/bin/activate
RUN pip3 install -r requirements.txt

EXPOSE 3001

# RUN python3 manage.py migrate

# CMD ["python3", "manage.py", "runserver", "3001"]