from django.apps import AppConfig


class MyPasswordManagerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'my_password_manager'
