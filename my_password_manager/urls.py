from django.urls import path
from my_password_manager import views

urlpatterns = [
    path('getRecord/', views.getRecords, name='get-record'),
    path('createRecord/', views.createRecord, name='create-record'),
    path('updateRecord/', views.updateRecord, name='update-record'),
    path('deleteRecord/', views.deleteRecord, name='delete-record'),
    
]

