from rest_framework import serializers
from .models import Record, CustomTextField
from django.contrib.auth.models import User

class CustomTextFieldSerializer(serializers.ModelSerializer):
    # Need this to explicitly make the field usable in nested serializer
    id =  serializers.IntegerField(required=False)
    
    class Meta:
        model = CustomTextField
        fields = ['field_type', 'field_name', 'field_value', 'id']

class RecordSerializer(serializers.ModelSerializer):
    customtextfields = CustomTextFieldSerializer(many=True)

    # Create record and its custom fields
    def create(self, validated_data):
        custom_textfields = validated_data.pop('customtextfields')
        record = Record.objects.create(**validated_data)
        for custom_textfield in custom_textfields:
            CustomTextField.objects.create(record=record, **custom_textfield)
        return record

    # update record and its custom fields
    def update(self, instance, validated_data):
        custom_textfields = validated_data.pop('customtextfields')
        record_instance = super().update(instance, validated_data)
        
        prev_custom_field_ids = CustomTextField.objects.filter(record=instance).values_list('id', flat=True)
        current_custom_field_ids = []
        
        for custom_textfield in custom_textfields:
            if custom_textfield.get('id') is not None:
                # if CustomTextField.objects.filter(pk=custom_textfield['id']).exists():
                    cf_instance = CustomTextField.objects.get(pk=custom_textfield['id'])
                    cf_instance.field_name = custom_textfield.get('field_name', cf_instance.field_name)
                    cf_instance.field_value = custom_textfield.get('field_value', cf_instance.field_value)
                    cf_instance.save()
                    current_custom_field_ids.append(cf_instance.id)
                # else: 
                    # continue
            else:
                cf_instance = CustomTextField.objects.create(record=record_instance, **custom_textfield)
                current_custom_field_ids.append(cf_instance.id)
                
        for id in prev_custom_field_ids:
            if id not in current_custom_field_ids:
                CustomTextField.objects.filter(pk=id).delete()
                
        return instance

    class Meta:
        model = Record
        fields = ['siteName',
                  'account',
                  'password',
                  'language',
                  'id',
                  'customtextfields'
                  ]
        
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username']

