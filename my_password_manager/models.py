from django.db import models
from django.db.models import Model, CharField, BooleanField, TextField

class Record(Model):
    siteName = CharField(max_length=100, blank=False)
    account = CharField(max_length=100, blank=False)
    password = CharField(max_length=100, blank=True)
    language = CharField(max_length=100, blank=True)

    def __str__(self):
        return self.siteName
    
class CustomTextField(Model):
    field_type = CharField(max_length=100, blank=False)
    field_name = CharField(max_length=100, blank=True)
    field_value = CharField(max_length=100, blank=False)
    record = models.ForeignKey(Record, related_name='customtextfields', on_delete=models.CASCADE)
    
    def __str__(self):
        return self.field_name
    
    