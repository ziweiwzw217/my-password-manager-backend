from django.contrib import admin
from .models import Record

class RecordAdmin(admin.ModelAdmin):
    list = ('siteName', 'account', 'password', 'language')
    
admin.site.register(Record, RecordAdmin)