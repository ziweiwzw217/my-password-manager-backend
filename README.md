# Getting Started 
For more information about the dependencies, check `requirements.txt`.

## Docker and Docker Compose
You need to download docker and docker-compose before proceeding. \
Also check out the Docker documentation [here](https://docs.docker.com/) 

## Run in Local Environment
The python library and its package manager need to be installed in advanced. Check [here](https://www.python.org/)

### Local Development Flow
After you pull the repo, `cd` to the directory root and run 
```
python3 -v venv env
source env/bin/activate
pip install -r requirement.txt
docker-compose -f docker-compose-local.yml
```
The commands above will create a virtual environment for your project such that your python packages installed within the project directory won't cause any confilcts to other python projects existing on your machine. \
The python packages are installed prior to running docker-compose command to guarentee that the packages will exist on both local machine and the container (see `docker-compose-local.yml`). For local development with docker, it is recommended to have them on local machine to faciliate development process. One of the benefits would be having auto-complete and references on objects provided by third-party libraries. 


#### Install Dependencies
`pip install -r requirements.txt` 

#### Migrate Changes
Go to the root directory that contains the file `manage.py`, run 
```
python3 manage.py makemigrations
python3 manage.py migrate
```

#### Run Server
`python3 manage.py runserver 3001`
You can also change the port number to any number you like. \
It will run the app in the development mode.\
Open [http://localhost:3001](http://localhost:3001) to view it in your browser. \
The page will reload when you make changes.

#### Create Superuser
`python3 manage.py createsuperuser` \
Make sure to do this after you install all the necessary dependencies.

## MongoDB
This project uses MongoDB as its database. For now, the host that is used to connect the databass is provided through environment variables. \
Therefore You need to create a `.env` file in the diretory that contains `settings.py` and specify the host within. Example:
```
HOST=127.0.0.1 //if you are running the project in local environment
//or
HOST=mongo //if you are running the project in Docker
```