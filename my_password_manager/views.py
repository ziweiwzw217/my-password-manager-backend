from telnetlib import RCP
from django.shortcuts import render

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from .models import Record

from .serializers import RecordSerializer, CustomTextFieldSerializer

@api_view(['GET'])
def getRecords(request):
    """
    Return the records whose site name cotains the query string. If an id is provided, return the record with that id and skip checking the site name
    """
    if request.method == 'GET':
        id = ''
        try:
            request.GET['id']
        except KeyError:
            pass
        else:
            id = request.GET['id']
            
        if(not id):
            try:
                request.GET['siteName']
            except KeyError:
                return Response({'message':'Key not found'}, status=status.HTTP_400_BAD_REQUEST)
            else:
                siteName = request.GET['siteName']

            if siteName == '':
                records = Record.objects.all()
            else:
                records = Record.objects.filter(siteName__icontains=siteName)
            
            if len(records) > 0:
                serializer = RecordSerializer(records, many=True)
                return Response(serializer.data, status=status.HTTP_200_OK)
            
            return Response(status=status.HTTP_204_NO_CONTENT)

        try:
            record = Record.objects.get(pk=id)
        except Record.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            serializer = RecordSerializer(record)
            return Response(serializer.data, status=status.HTTP_200_OK)
        
    return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def createRecord(request):
    """
    Create a new record using the data sent from the frontend if valid
    """
    if request.method == 'POST':
        try:
            isValid = request.data['account'] and request.data['siteName']
        except KeyError:
            return Response({'message':'Key not found'}, status=status.HTTP_400_BAD_REQUEST)            
        else:
            account = request.data['account']
            siteName = request.data['siteName']
            
        try:
            records = Record.objects.get(account=account, siteName=siteName)
        except Record.DoesNotExist:
            serializer = RecordSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(status=status.HTTP_201_CREATED)

            return Response({'message':'Invalid Data'}, status=status.HTTP_400_BAD_REQUEST)

        else:
            return Response({'message':'A record with this account and site name already exists'},status=status.HTTP_200_OK)
    return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view(['PUT'])
def updateRecord(request):
    """
    Update a record using the data sent from the frontend if valid
    """
    if request.method == 'PUT':
        try:
            isValid = request.data['id'] and request.data['account'] and request.data['siteName']
        except KeyError:
            return Response({'message':'Key not found'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            id = request.data['id']
            
        try:
            record = Record.objects.get(pk=id) # Mongo's _id could not be understood by Django, thus we use navtive Django identifier.
        except Record.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            serializer = RecordSerializer(record, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(status=status.HTTP_202_ACCEPTED)
            
            return Response({'message':'Invalid Data'}, status=status.HTTP_400_BAD_REQUEST)

    return Response(status=status.HTTP_400_BAD_REQUEST)
    
@api_view(['DELETE'])
def deleteRecord(request):
    """
    Delete one or more record using the list of ids sent from the frontend if valid
    The input list must be an iterable python list. If a record with an id is not found, it will be 
    sent along with the response. If all ids are found, an empty list is sent.
    """
    if request.method == 'DELETE':
        try:
            isValid = request.data['id']
        except KeyError:
            return Response({'message':'Key not found'},status=status.HTTP_400_BAD_REQUEST)
        else:
            ids = request.data['id']
        
        if not isinstance(ids, list):
            return Response({'message': 'Not a list'}, status=status.HTTP_400_BAD_REQUEST)
        
        invalidId = []
        deletedId = []
        for id in ids:
            try:
                record = Record.objects.get(pk=id)
            except Record.DoesNotExist:
                invalidId.append(id)
            else:
                record.delete()
                deletedId.append(id)
                
        if len(invalidId) > 0:
            return Response(invalidId, status=status.HTTP_200_OK)
            
        return Response(deletedId, status=status.HTTP_204_NO_CONTENT)
            
    
    return Response(status=status.HTTP_400_BAD_REQUEST)
           